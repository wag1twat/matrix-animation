import { Box, Text } from '@chakra-ui/react';
import React from 'react';

type MatrixProps = {};

const str =
  'ぁぃぅぇぉかきくけこんさしすせそたちつってとゐなにぬねのはひふへほゑまみむめもゃゅょゎをァィゥヴェォカヵキクケヶコサシスセソタチツッテトヰンナニヌネノハヒフヘホヱマミムメモャュョヮヲㄅㄉㄓㄚㄞㄢㄦㄆㄊㄍㄐㄔㄗㄧㄛㄟㄣㄇㄋㄎㄑㄕㄘㄨㄜㄠㄤㄈㄏㄒㄖㄙㄩㄝㄡㄥabcdefghigklmnopqrstuvwxyz123456789%@#$<>^&*_+';

function getRandomInt(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

const texts = Array.from(Array(50).keys());

const keyframes = texts.reduce((acc, current, index) => {
  const charts = getRandomInt(8, str.length + 1);

  const chartsSymbols = Array.from(Array(charts).keys()).map(() =>
    getRandomInt(0, str.length + 1),
  );

  const text = chartsSymbols.map((i) => str[i]).join('');

  const beforeN1 = getRandomInt(3, 6);

  const beforeN2 = getRandomInt(3, 6);

  const afterN1 = getRandomInt(3, 6);

  const afterN2 = getRandomInt(3, 6);

  return {
    ...acc,
    [`@keyframes typing-${index}`]: {
      '0%': {
        height: 0,
      },
      '25%': {
        height: '100%',
      },
      '100%': {
        height: '100%',
        content: `'${text}'`,
      },
    },
    [`p:nth-child(${index})::before`]: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      height: '100%',
      overflow: 'hidden',
      content: `'${text}'`,
      color: '#b3ffc7',
      textShadow:
        '0 0 1px #fff, 0 0 2px #fff, 0 0 5px currentColor, 0 0 10px currentColor',
      animation: `typing-${index} ${beforeN1}s steps(20, end) ${-beforeN2}s infinite`,
      zIndex: 1,
    },
    [`p:nth-child(${index})::after`]: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      height: '100%',
      overflow: 'hidden',
      content: `''`,
      background:
        'linear-gradient(rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.89), rgba(0, 0, 0, 0.89), transparent 75%, transparent)',
      backgroundSize: '100% 220%',
      backgroundRepeat: 'no-repeat',
      animation: `mask ${afterN1}s infinite ${-afterN2}s linear`,
      zIndex: 2,
    },
  };
}, {});

const Matrix: React.FC<MatrixProps> = React.memo((props) => {
  const ref = React.useRef<HTMLDivElement | null>(null);

  return (
    <Box
      ref={ref}
      __css={{
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        flexWrap: 'nowrap',
        flexDirection: 'row',
        fontFamily: 'Roboto, monospace, sans-serif',
        background: '#000',
        '@keyframes mask': {
          '0%': {
            backgroundPosition: '0 220%',
          },
          '30%': {
            backgroundPosition: '0 0%',
          },
          '100%': {
            backgroundPosition: '0 0%',
          },
        },
        ...keyframes,
      }}
    >
      {texts.map((key) => {
        return (
          <Text
            key={key}
            position="relative"
            height="100vh"
            textAlign="center"
            wordBreak="break-all"
            whiteSpace="pre-wrap"
            width="16px"
            fontSize="16px"
          />
        );
      })}
    </Box>
  );
});

export { Matrix };
